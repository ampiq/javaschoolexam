package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    public static boolean isGoodSizeForMakingPyramid(int size) {
        int i = 1;
        while (size > 0){
            size = size - i;
            i++;
        }
        return size == 0;
    }

    /**
     * Builds a pyramid with sorted values (with minimum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (isGoodSizeForMakingPyramid(inputNumbers.size()) & !inputNumbers.contains(null)) {
            int rows = 0;
            int columns;
            int size = inputNumbers.size();
            Collections.sort(inputNumbers);
            while (size > 0) {
                size = size - rows;
                rows++;
            }
            rows = rows - 1;
            columns = 2 * rows - 1;
            int[][] outputMatrix = new int[rows][columns];
            Arrays.deepToString(outputMatrix);
            int currentCenter = (columns / 2);
            int numberOfElementsInARow = 1;
            int index = 0;

            for (int i = 0, offset = 0; i < rows; i++, offset++, numberOfElementsInARow++) {
                int start = currentCenter - offset;
                for (int j = 0; j < numberOfElementsInARow * 2; j += 2, index++) {
                    outputMatrix[i][start + j] = inputNumbers.get(index);
                }
            }
            System.out.println(Arrays.deepToString(outputMatrix));
            return outputMatrix;
        } else {
            throw new CannotBuildPyramidException("You can't build a pyramid");
        }
    }
}

