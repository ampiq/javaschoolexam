package com.tsystems.javaschool.tasks.pyramid;

public class CannotBuildPyramidException extends RuntimeException {

    private int errorCode;


    public CannotBuildPyramidException(String message)
    {
        super(message);
    }

    public CannotBuildPyramidException(int errorCode, String message)
    {
        super(message);
        this.errorCode = errorCode;
    }

    public int getErrorCode()
    {
        return errorCode;
    }
}