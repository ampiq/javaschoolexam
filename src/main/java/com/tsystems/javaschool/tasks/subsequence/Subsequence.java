package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence{

    //LCS-algorithm
    public static int getLongestCommonSubsequence(List a, List b){
        int firstLength = a.size();
        int secondLength = b.size();
        int[][] matrixLCS = new int[firstLength+1][secondLength+1];
        for(int i=0; i<=firstLength; i++){
            for(int j=0; j<=secondLength; j++){
                if(i==0 || j==0){
                    matrixLCS[i][j]=0;
                }else if(a.get(i-1)==b.get(j-1)){
                    matrixLCS[i][j] = 1 + matrixLCS[i-1][j-1];
                }else{
                    matrixLCS[i][j] = Math.max(matrixLCS[i-1][j], matrixLCS[i][j-1]);
                }
            }
        }
        return matrixLCS[firstLength][secondLength];
    }

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null || x.contains(null) || y.contains(null)){
            throw new IllegalArgumentException("Not null parameter");
        } else {
            if (y.size() < x.size()) {
                return false;
            } else if (getLongestCommonSubsequence(x, y) == x.size()) {
                return true;
            }
        }
        return false;
    }
}